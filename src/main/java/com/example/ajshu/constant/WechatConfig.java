package com.example.ajshu.constant;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.text.SimpleDateFormat;
import java.util.UUID;

/**
 * Created by shu.aojun@qq.com on 2016/11/9-22:05
 * Copyright 2016 ao jun shu
 */
@ConfigurationProperties(prefix = "wechat")
public class WechatConfig {
    private String appid;
    private String mch_id;
    private String device_info;
    private String nonce_str;//随机数
    private String time_start;
    private String time_expire;
    private String sign;//*签名
    private String body;//商品描述 商品简单描述，该字段须严格按照规范传递，（浏览器打开的移动网页的主页title名-商品概述）（腾讯充值中心-QQ会员充值）
    private String out_trade_no;//订单号
    private Integer total_fee;//*总金额
    private String spbill_create_ip;//*终端IP
    private String notify_url="";//*通知地址   接收微信支付异步通知回调地址，通知url必须为直接可访问的url，不能携带参数。
    private String trade_type = "";//
    private String product_id;//*商品ID
    private String limit_pay = "";//no_credit--指定不能使用信用卡支付
    private String openid;//*

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getMch_id() {
        return mch_id;
    }

    public void setMch_id(String mch_id) {
        this.mch_id = mch_id;
    }

    public String getDevice_info() {
        return device_info;
    }

    public void setDevice_info(String device_info) {
        this.device_info = device_info;
    }

    public String getSign() {
        return sign;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getOut_trade_no() {
        return out_trade_no;
    }

    public String getNonce_str() {
        return nonce_str;
    }

    public void setNonce_str(String nonce_str) {
        this.nonce_str = nonce_str;
    }

    public void setOut_trade_no(String out_trade_no) {
        this.out_trade_no = out_trade_no;
    }

    public String getTime_start() {
        return time_start;
    }

    public void setTime_start(String time_start) {
        this.time_start = time_start;
    }

    public String getTime_expire() {
        return time_expire;
    }

    public void setTime_expire(String time_expire) {
        this.time_expire = time_expire;
    }

    public Integer getTotal_fee() {
        return total_fee;
    }

    public void setTotal_fee(Integer total_fee) {
        this.total_fee = total_fee;
    }

    public String getSpbill_create_ip() {
        return spbill_create_ip;
    }

    public void setSpbill_create_ip(String spbill_create_ip) {
        this.spbill_create_ip = spbill_create_ip;
    }

    public String getNotify_url() {
        return notify_url;
    }

    public void setNotify_url(String notify_url) {
        this.notify_url = notify_url;
    }

    public String getTrade_type() {
        return trade_type;
    }

    public void setTrade_type(String trade_type) {
        this.trade_type = trade_type;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getLimit_pay() {
        return limit_pay;
    }

    public void setLimit_pay(String limit_pay) {
        this.limit_pay = limit_pay;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("appid", appid)
                .append("mch_id", mch_id)
                .append("device_info", device_info)
                .append("nonce_str", nonce_str)
                .append("sign", sign)
                .append("body", body)
                .append("out_trade_no", out_trade_no)
                .append("total_fee", total_fee)
                .append("spbill_create_ip", spbill_create_ip)
                .append("notify_url", notify_url)
                .append("trade_type", trade_type)
                .append("product_id", product_id)
                .append("limit_pay", limit_pay)
                .append("openid", openid)
                .toString();
    }
}

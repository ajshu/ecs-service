package com.example.ajshu.constant;

/**
 * Created by shu.aojun@qq.com on 2016/10/27-22:22
 * Copyright 2016 ao jun shu
 */
public class Global {

    public static String appId = "wxb990ce7ca437f287";
    public static String appSecret = "a9a677e62e559ed5185ac50229623ecf";
    //获取token
    public static String getToken = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wxb990ce7ca437f287&secret=a9a677e62e559ed5185ac50229623ecf";
    //通过code换取网页授权access_token
    public static String getTokenByCode="https://api.weixin.qq.com/sns/oauth2/access_token?appid=wxb990ce7ca437f287&secret=a9a677e62e559ed5185ac50229623ecf&code=%s&grant_type=authorization_code";
    //获取关注者列表接口
    public static String getUserList="https://api.weixin.qq.com/cgi-bin/user/get?access_token=%s&next_openid=";
    //获取用户信息
    public static String getUserInfo="https://api.weixin.qq.com/cgi-bin/user/info?access_token=%s&openid=%s";
    //创建二维码接口
    public static String getUserQrcode="https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=%s";
    //二维码路径
    public static String Qrcode = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=%s";
    //JSAPI 生产
    public static String jsapiTicket = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=%s&type=jsapi";
}

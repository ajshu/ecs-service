package com.example.ajshu.quartz;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 描述：描述这个类的功能
 * 创建人：ajshu@iflytek.com
 * 创建时间：2016/9/28-14:34
 * 备注：
 *  fixedDelay表示当方法执行完毕5000ms后，Spring scheduling会再次调用该方法
 *  fixedRate = 5000表示每隔5000ms，Spring scheduling会调用一次该方法，不论该方法的执行时间是多少
 *  cron = * /5 * * * * * *提供了一种通用的定时任务表达式，这里表示每隔5秒执行一次
 */

@Component
public class MovieQuartz {


    @Scheduled(fixedDelay = 1000 * 300)
    public void updateStills(){
        System.out.println(111111111);
    }

    /**
     * 每小时刷新token
     */
    @Scheduled(cron = "0 0 0/1 * * ?")
    public void refreshToken(){
        System.out.println(111111);
    }

}

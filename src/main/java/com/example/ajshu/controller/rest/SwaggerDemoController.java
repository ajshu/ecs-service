package com.example.ajshu.controller.rest;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 描述：描述这个类的功能
 * 创建人：ajshu@iflytek.com
 * 创建时间：2016/8/1-16:55
 * 备注：
 */
@RestController
@RequestMapping(value="/demo/user")
@Api(value = "用户接口",description = "用户相关类接口服务")
public class SwaggerDemoController {


    /*
     *  http://localhost:8001/swagger-ui.html
     */

    /**
     */
    @ApiOperation(value="获取用户信息接口",notes="获取用户信息接口")
    @RequestMapping(value = "/all", method= RequestMethod.POST)
    public JSONObject getUsers(){
        JSONObject object = new JSONObject();
        return object;
    }

    @ApiOperation(value="获取单条信息2",notes="获取单条信息")
    @RequestMapping(value = "/one", method= RequestMethod.POST)
    public JSONObject getOneUsers(@ApiParam(value = "参数的描述ID") String id, @ApiParam(value = "参数的描述姓名")String name){
        JSONObject object = new JSONObject();

        return object;
    }

}

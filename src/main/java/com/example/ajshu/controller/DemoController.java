package com.example.ajshu.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 描述：描述这个类的功能
 * 创建人：ajshu@iflytek.com
 * 创建时间：2016/9/1-15:34
 * 备注：spring data jpa
 * 1:分页排序测试
 * 2:动态查询
 */

@RestController
@RequestMapping("/rest/demo")
public class DemoController {

    /*@Autowired
    DemoService demoService;


    @ApiOperation(value="获取用户信息接口",notes="支持排序功能")
    @RequestMapping(value = "/sort",method= RequestMethod.GET)
    public JSONObject getUserInfo(){
        JSONObject object = new JSONObject();
        //默认升序  Sort.Direction.ASC 多个字段排序
        Sort sort = new Sort(Sort.Direction.DESC,"createTime","phone");

        object.put("data", demoService.findAll(sort));
        return object;
    }

    @RequestMapping(value = "/page",method= RequestMethod.GET)
    public JSONObject getUserPage(int page,int size){
        JSONObject object = new JSONObject();
        //分页：构造一个PageRequest,构造方法
        //PageRequest(int page, int size, Direction direction, String... properties)
        //PageRequest(int page, int size, Sort sort)
        //PageRequest(int page, int size)

        PageRequest pageRequest = new PageRequest(page,size,Sort.Direction.ASC,"id","phone");

        //返回Page 对象
        Page<User> listPage = demoService.findAll(pageRequest);
        object.put("data",listPage);

        return object;
    }

    @RequestMapping(value = "/like",method= RequestMethod.GET)
    public JSONObject getDetPage(){
        JSONObject object = new JSONObject();

        PageRequest pageRequest = new PageRequest(0,10);
        //返回Page 对象
        Page<User> listPage = demoService.findByLoginNameLike(pageRequest,"%math%");
        object.put("data",listPage);

        return object;
    }

    @RequestMapping(value = "/save",method= RequestMethod.GET)
    public JSONObject saveUser(){
        JSONObject object = new JSONObject();
        User user = new User();

        Random random = new Random();
        user.setLoginName("math" + (random.nextInt(1000)));
        user.setPhone("18226612055");
        user.setAppName("rrt");
        object.put("data", demoService.saveUser(user));
        return object;
    }*/


    /*@RequestMapping("list/data") JpaSpecificationExecutor
    public String list(int page, int size, Model model, HttpServletRequest request){
        Map<String,Object> map = new HashMap<>();
        map.put("skip",page);
        map.put("limit",size);
        map.put("title",request.getParameter("title"));

        String title = request.getParameter("title");
        String addTime = request.getParameter("addTime");

        Specification specification = new Specification<TaobaoEntity>() {
            @Override
            public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();

                if(StringUtils.isNotEmpty(addTime)){
                    //某条数据的创建日期小于今天。
                    predicates.add(criteriaBuilder.lessThan(root.get("add_time"), addTime));
                }
                if(StringUtils.isNotEmpty(title)){
                    //like 查询
                    predicates.add(criteriaBuilder.like(root.get("title"), "%"+title+"%"));
                }
                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        };

        PageRequest pageRequest = new PageRequest(page,size, Sort.Direction.ASC,"desc");
        Page entityPage = repository.findAll(specification,pageRequest);

        return "taobao/data";*/
}

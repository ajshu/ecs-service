package com.example.ajshu.common.http;

import java.io.*;
import java.net.URL;
import java.net.URLDecoder;
import java.util.*;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by shu.aojun@qq.com on 2016/10/24-23:02
 * Copyright 2016 ao jun shu
 */
public class HttpClientUtil {
    public static String doPost(String url){
        HttpClient httpClient;
        HttpPost httpPost;
        String result = null;
        String charset = "utf-8";
        try{
            httpClient = new SSLClient();
            httpPost = new HttpPost(URLDecoder.decode(url,charset));
            /*//设置参数
            List<NameValuePair> list = new ArrayList<>();
            Map<String,String> createMap = new HashMap<>();
            createMap.put("authuser","*****");
            createMap.put("authpass","*****");
            createMap.put("orgkey","****");
            createMap.put("orgname","****");
            Iterator iterator = createMap.entrySet().iterator();
            while(iterator.hasNext()){
                Entry<String,String> elem = (Entry<String, String>) iterator.next();
                list.add(new BasicNameValuePair(elem.getKey(),elem.getValue()));
            }
            if(list.size() > 0){
                UrlEncodedFormEntity entity = new UrlEncodedFormEntity(list,charset);
                httpPost.setEntity(entity);
            }*/
            HttpResponse response = httpClient.execute(httpPost);
            if(response != null){
                HttpEntity resEntity = response.getEntity();
                if(resEntity != null){
                    result = EntityUtils.toString(resEntity,charset);
                }
            }

        }catch(Exception ex){
            ex.printStackTrace();
        }
        return result;
    }

    //JSON 参数形式
    public static String doPost(String url, String content){
        HttpClient httpClient;
        HttpPost httpPost;
        String result = null;
        String charset = "utf-8";
        try{
            httpClient = new SSLClient();
            httpPost = new HttpPost(URLDecoder.decode(url,charset));
            //设置参数
            StringEntity myEntity = new StringEntity(content,ContentType.APPLICATION_JSON);// 构造请求数据
            httpPost.setEntity(myEntity);

            HttpResponse response = httpClient.execute(httpPost);
            if(response != null){
                HttpEntity resEntity = response.getEntity();
                if(resEntity != null){
                    result = EntityUtils.toString(resEntity,charset);
                }
            }

        }catch(Exception ex){
            ex.printStackTrace();
        }
        System.out.println(result);
        return result;
    }

    public static String doPostXml(String url ,String xml){
        HttpClient httpClient;
        HttpPost httpPost;
        String result = null;
        String charset = "utf-8";
        try{
            httpClient = new SSLClient();
            httpPost = new HttpPost(URLDecoder.decode(url,charset));
            //设置参数
            StringEntity myEntity = new StringEntity(xml,ContentType.APPLICATION_XML);// 构造请求数据
            httpPost.setEntity(myEntity);

            HttpResponse response = httpClient.execute(httpPost);
            if(response != null){
                HttpEntity resEntity = response.getEntity();
                if(resEntity != null){
                    result = EntityUtils.toString(resEntity,charset);
                }
            }

        }catch(Exception ex){
            ex.printStackTrace();
        }
        System.out.println(result);
        return result;
    }

    private static StringBuffer httpsRequest(String requestUrl, String requestMethod, String output) {
        StringBuffer buffer = new StringBuffer();
        try {
            URL url = new URL(requestUrl);
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod(requestMethod);
            if (null != output) {
                OutputStream outputStream = connection.getOutputStream();
                outputStream.write(output.getBytes("UTF-8"));
                outputStream.close();
            }
            // 从输入流读取返回内容
            InputStream inputStream = connection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String str;
            while ((str = bufferedReader.readLine()) != null) {
                buffer.append(str);
            }
            bufferedReader.close();
            inputStreamReader.close();
            inputStream.close();
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return buffer;
    }

    public static String httpsRequest(String requestUrl, String output){
       return httpsRequest(requestUrl,"POST",output).toString();
    }
}

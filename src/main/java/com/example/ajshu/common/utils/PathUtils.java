package com.example.ajshu.common.utils;


import java.io.File;
import java.util.UUID;

public class PathUtils {

    static String root = "/usr/local/upload/";

    /**
     * 在原根目录下生成日期格式文件夹
     * @return
     */
    public static String rootPath(){
        File pathFile = new File(root);
        if (!pathFile.exists()) {
            boolean t = pathFile.mkdirs();
        }
        return root;
    }

    /**
     * 重命名文件名
     * @param fileName 原文件名
     */
    public static String rename(String fileName) {
        return UUID.randomUUID() + "." + suffix(fileName);
    }

    /**
     * 获取文件后缀名
     * @param fileName
     * @return
     */
    public static String suffix(String fileName) {
        return fileName.substring(fileName.lastIndexOf(".") + 1);
    }

    /**
     * 拼接路径 添加或删除多余的“/”
     * 如“/././”
     * @param path1
     * @param path2
     * @return
     */
    public static String concat(String path1, String path2) {

        return path1 + path2;
    }

}

package com.example.ajshu;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

/**
 * Created by shu.aojun@qq.com on 2017/2/6-22:12
 * Copyright 2017 ao jun shu
 */
public class ServletInitializer extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SpringBootJpaApplication.class);
    }
}

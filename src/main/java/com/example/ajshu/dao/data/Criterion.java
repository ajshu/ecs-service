package com.example.ajshu.dao.data;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * 描述：描述这个类的功能
 * 创建人：ajshu@iflytek.com
 * 创建时间：2016/10/17-13:35
 * 备注：
 */


public interface Criterion {

    enum Operator {
        EQ, NE, LIKE, GT, LT, GTE, LTE, AND, OR
    }

    Predicate toPredicate(Root<?> root, CriteriaQuery<?> query,
                                 CriteriaBuilder builder);
}
